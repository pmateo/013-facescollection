//
//  PersonaCollectionViewCell.swift
//  FacesCollection
//
//  Created by Pablo Mateo Fernández on 22/01/2017.
//  Copyright © 2017 355 Berry Street S.L. All rights reserved.
//

import UIKit

class PersonaCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var name: UILabel!
    
}
